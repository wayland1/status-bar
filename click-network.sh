#!/bin/sh
if [[ $(ps -e | grep nm-applet | wc -l) == 0 ]]; then
	nm-applet    2>&1 >/dev/null &
	sleep 1
fi
~/.config/sway/networkmanager_dmenu
