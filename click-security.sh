#!/bin/sh

update=$(pacman -Qu | wc -l)
ico="🛡️"
msg="Good ! System already up-to-date."

if [[ $update > 0 ]]; then
	ico="🔴"
	msg="$update update(s) available. Please upgrade your system."
fi

notify-send -t 10000 "$ico System Update" "$msg"

#read -n 1 -r -s
