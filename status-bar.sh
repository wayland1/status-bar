#!/bin/bash

##### Variables
# Updates frequence (seconds)
update_freq=5
pubip_freq=10
uptime_freq=60
updrep_freq=300
# Temporary files
updfile="/tmp/.updrc"
pubipfile="/tmp/.pubiprc"
uptfile="/tmp/.uptrc"
timefile="/tmp/.timerc"
# Debug mode
debug=1
debuglog="/tmp/status-bar.debug"
# No need to update these items each seconds
# Kernel ⚙🐧  
uname=" "$(uname -r)
# Keyring icon 🔑
keyring=""
# Bluetooth icon 🕸
bluetooth=""
# Security status 💣🚀
security=""
msg="No check performed yet... Please be patient and come back later..."
# Public ip icon
pubip=""
  

##### Functions 🔴⭕🚨🔥🛡️    
function check_updates {
  upd=$(pacman -Qu | wc -l)
  if [[ $upd > 0 ]]; then
    security="🔥"
    msg="$upd update(s) available. Please upgrade your system."
  else
    security=""
    msg="Good ! System already up-to-date."
  fi
  printf "upd=\"$upd\"\nsecurity=\"$security\"\nmsg=\"$msg\"\n" > $updfile
}

# Public ip 
function check_pubip {
  #pubip=" "$(curl ifconfig.co)
  #pubip=" "$(curl ifconfig.me)
  public_ip=" "$(dig +short myip.opendns.com @resolver1.opendns.com)
  printf "pubip=\"$public_ip\"\n" > $pubipfile
}

# 📅🌜🌞🌄   
function check_date {
  date=""$(date +' %A %d/%m/%Y %V')
  h=$(date "+%H")
  if (($h>=21 || $h<=6)); then
    tod=" "
  #elif (($h>=11 && $h<=16)); then
  #  tod=" "
  else
    tod=" "
  fi
  printf "date=\"$date\"\ntod=\"$tod\"\n" > $timefile
}

# 🚀
function check_uptime {
  upt=" "$(uptime -p | awk '{ sub(/.*/,"",$1); \
	                      sub(/^\s*/,""); \
                              sub(/\s*day(s)?,\s*/,"j"); \
			      sub(/\s*hour(s)?,\s*/,"h"); \
			      sub(/\s*minute(s)?\s*/,"min"); \
			      print }')
  printf "uptime=\"$upt\"\n" > $uptfile
}

function print_status {
  # Chrono init
  [[ $debug -gt 0 ]] && start_time=$(($(date +%s%N)/1000000))

  # Network 📡🔒🖧  
  #network_info="$ips_and_interfaces→ $default_gateway $ssid"
  network=$(ip -o -4 a | awk 'NR>1 {print ($2~/wl/?"":($2~/tun/?" ":"🖧"))" "$2""$4}' | tr '\n' ' ' | awk 'END {sub(/\s*$/,"");print !NR?"🖧":$0}')

  # Battery 🔋🔌⚡   
  bat_capacity=$(cat /sys/class/power_supply/cw2015-battery/capacity)
  #bat_status=$(cat /sys/class/power_supply/cw2015-battery/status)
  #if (($bat_capacity>=95)); then
  #  bat_icon=" "
  #elif (($bat_capacity>=70)); then
  #  bat_icon=" "
  #elif (($bat_capacity>=45)); then
  #  bat_icon=" "
  #elif (($bat_capacity>=25)); then
  #  bat_icon=" "
  #else
  #  bat_icon=" "
  #fi
  case $((
      $bat_capacity >= 0 && $bat_capacity <= 20 ? 1 :
      $bat_capacity > 20 && $bat_capacity <= 40 ? 2 :
      $bat_capacity > 40 && $bat_capacity <= 60 ? 3 :
      $bat_capacity > 60 && $bat_capacity <= 80 ? 4 : 5
    )) in
    (1) bat_icon=" ";;
    (2) bat_icon=" ";;
    (3) bat_icon=" ";;
    (4) bat_icon=" ";;
    (5) bat_icon=" ";;
  esac
  #battery=$(cat /sys/class/power_supply/cw2015-battery/status | awk -v ico=$bat_icon -v bat=$bat_capacity '{print ($1=="Charging"?"":ico)" "bat"% ("$1")"}')
  battery=$(cat /sys/class/power_supply/cw2015-battery/status | awk -v ico=$bat_icon -v bat=$bat_capacity '{print ($1=="Charging"?"":ico)" "bat"%"}')

  # Audio 🔉🔇🔈   
  volume=$(pamixer --get-volume)
  audio=$(pamixer --get-mute | awk -v vol=$volume '{print $1=="true"?"":" "vol"%"}')

  # Uptime/PublicIP/Date/Time/Security
  [[ -f $uptfile ]] && source $uptfile
  [[ -f $pubipfile ]] && source $pubipfile
  [[ -f $timefile ]] && source $timefile
  [[ -f $updfile ]] && source $updfile

  # Chrono end
  [[ $debug -gt 0 ]] && chrono=$(($(date +%s%N)/1000000 - $start_time))

  # Send JSON blocks
  echo ',['
  echo "{\"name\":\"id_network\",\"full_text\":\"$network  $pubip \"},"
  echo "{\"name\":\"id_uptime\",\"full_text\":\"$uptime \"},"
  echo "{\"name\":\"id_system\",\"full_text\":\"$uname \"},"
  echo "{\"name\":\"id_battery\",\"full_text\":\"$battery \"},"
  echo "{\"name\":\"id_audio\",\"full_text\":\"$audio \"},"
  echo "{\"name\":\"id_date\",\"full_text\":\"$date \"},"
  echo "{\"name\":\"id_time\",\"full_text\":\"$tod$(date +'%H:%M:%S') \"},"
  echo "{\"name\":\"id_bluetooth\",\"full_text\":\"$bluetooth\"},"
  echo "{\"name\":\"id_keyring\",\"full_text\":\"$keyring\"},"
  echo "{\"name\":\"id_security\",\"full_text\":\"$security\"},"
  echo "{\"name\":\"id_chronometer\",\"full_text\":\"$chrono\"}"
  echo ']'

}

###############################################################################
##### Do the urgent job NOW
# Send the header so that sway-bar knows we want to use JSON:
echo '{ "version": 1, "click_events":true }'
echo '['
echo '[]'
# Print status bar
(while true; do print_status; sleep 1; done) &
#
##### security checks
# Update security status in background on a time basis
(while true; do check_updates; sleep $update_freq; done) &
# Update public ip in background on a time basis
(while true; do check_pubip; sleep $pubip_freq; done) &
# Update uptime status in background on a time basis
(while true; do check_uptime; sleep $uptime_freq; done) &
# Update pacman repositories and time of day icon in background on a time basis
(while true; do check_date; sudo /usr/bin/pacman -Sy >/dev/null 2>&1; sleep $updrep_freq; done) &

##### Job done! Now wait for user input and pray ;)
# Listen for STDIN events
while read line;
do
	# -- DEBUG
	[[ $debug == "1" ]] && printf "$line\n" >> $debuglog
	# -- DEBUG

	if [[ $line == *"name"*"id_network"*"button"*"1"*"event"* ]]; then
		~/.config/sway/sway-bar/click-network.sh
	fi
	if [[ $line == *"name"*"id_uptime"*"button"*"1"*"event"* ]]; then
		#termite -e ytop &
		urxvt -fn "xft:DejaVu Sans Mono:size=10" \
		      -name "floating_term" \
		      -geometry 100x40 \
		      -bg Grey25 \
		      -fg lightgrey \
		      +sb \
		      -e ~/.config/sway/sway-bar/click-uptime.sh &
	fi
	if [[ $line == *"name"*"id_system"*"button"*"1"*"event"* ]]; then
		urxvt -fn "xft:SourceCodePro:size=10" \
		      -name "floating_term" \
		      -geometry 83x23 \
		      -bg black \
		      -fg lightgrey \
		      +sb \
		      -e ~/.config/sway/sway-bar/click-system.sh &
	fi
	if [[ $line == *"name"*"id_battery"*"button"*"1"*"event"* ]]; then
		notify-send -t 10000 "🔋 Status" "Battery block OK"
	fi
	# Bouton Gauche : ,{ "name": "id_audio", "button": 1, "event": 272, ... }
	if [[ $line == *"name"*"id_audio"*"button"*"1"*"event"* ]]; then
		pamixer --toggle-mute
	fi
	# Bouton Milieu : ,{ "name": "id_audio", "button": 2, "event": 274, ... }
	if [[ $line == *"name"*"id_audio"*"button"*"2"*"event"* ]]; then
		pamixer --toggle-mute
	fi
	# Bouton Droit :  ,{ "name": "id_audio", "button": 3, "event": 273, ... }
	if [[ $line == *"name"*"id_audio"*"button"*"3"*"event"* ]]; then
		urxvt -fn "xft:SourceCodePro:size=10" \
		      -name "floating_term" \
		      -geometry 80x20 \
		      -bg black \
		      -fg grey \
		      +sb \
		      -e alsamixer
	fi
	# Molette Haut :  ,{ "name": "id_audio", "button": 4, "event": 768, ... }
	if [[ $line == *"name"*"id_audio"*"button"*"4"*"event"* ]]; then
		pamixer -i 1
	fi
	# Molette Bas :   ,{ "name": "id_audio", "button": 5, "event": 769, ... }
	if [[ $line == *"name"*"id_audio"*"button"*"5"*"event"* ]]; then
		pamixer -d 1
	fi
	if [[ $line == *"name"*"id_bluetooth"*"button"*"1"*"event"* ]]; then
		blueman-manager >/dev/null
	fi
	if [[ $line == *"name"*"id_date"*"button"*"1"*"event"* ]]; then
		urxvt -fn "xft:SourceCodePro:size=10" \
		      -name "floating_term" \
		      -geometry 75x35 \
		      -bg black \
		      -fg grey \
		      +sb \
		      -e ~/.config/sway/sway-bar/click-date.sh &
	fi
	if [[ $line == *"name"*"id_time"*"button"*"1"*"event"* ]]; then
		urxvt -fn "xft:SourceCodePro:size=10" \
		      -name "floating_term" \
		      -geometry 125x40 \
		      -bg black \
		      -fg grey \
		      +sb \
		      -e ~/.config/sway/sway-bar/click-time.sh &
	fi
	if [[ $line == *"name"*"id_keyring"*"button"*"1"*"event"* ]]; then
		seahorse
	fi
	if [[ $line == *"name"*"id_security"*"button"*"1"*"event"* ]]; then
		[[ -f $updfile ]] && . $updfile
		notify-send -t 10000 "$security Security status" "$msg"
	fi
done
